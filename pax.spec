Name:           pax
Version:        3.4
Release:        36
Summary:        POSIX File System Archiver
License:        BSD
URL:            http://repository.timesys.com/buildsources/p
Source:         http://repository.timesys.com/buildsources/p/pax/pax-3.4/pax-3.4.tar.bz2
Patch0000:      pax-disable_Werror_option.patch
Patch0001:      pax-3.4-gcc10.patch

%description
Pax is the POSIX standard archive tool. It supports the two most common
forms of standard archive (backup) files - CPIO and TAR.

%package        help
Summary:        Help documents for pax
Requires:       %{name} = %{version}-%{release}
BuildRequires:  gcc

%description    help
Man pages and other related help documents for pax.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install

%files
%license COPYING
%doc ABOUT-NLS AUTHORS
%{_bindir}/pax

%files help
%doc ChangeLog NEWS README THANKS
%doc %{_mandir}/man1/pax.1.gz

%changelog
* Fri 30 Jul 2021 zhangtao<zhangtao221@huawei.com> - 3.4-36
- fix gcc upgrade causes compilation failure

* Web 02 Jun 2021 zhaoyao<zhaoyao32@huawei.com> - 3.4-35
- fixs faileds: /bin/sh: gcc: command not found.

* Tue Feb 18 2020 Ling Yang <lingyang2@huawei.com> - 3.4-34
- Package init
